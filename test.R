setwd("H:/machine learning/data")

dataFrame.raw <- read.csv('students.csv')
dataFrame <- as.data.frame(dataFrame.raw)
str(dataFrame)
summary(dataFrame)
dim(dataFrame)

##Q1## 

install.packages('ggplot2')
library(ggplot2)

any(is.na(dataFrame))#there is no NA

#higher affect
ggplot(dataFrame, aes(higher,G3))  + geom_boxplot(aes(colour = factor(higher)))

##Q2## 

#school affect
ggplot(dataFrame, aes(school,G3))  + geom_boxplot(aes(colour = factor(school)))

#another option
ggplot(dataFrame, aes(school, fill = G3)) + geom_bar()
ggplot(dataFrame, aes(G3, fill = school)) + geom_bar(position = 'fill')

#absences histogram
ggplot(dataFrame,aes(absences)) +geom_histogram(aes(position = 'fill'),color='black' ,bins=50 ,alpha=0.5) +theme_bw()
#function to remove absences>35
coercex <- function(x,by){
  if(x<=by) return(x) 
  return(by)
}

dataFrame$absences <- sapply(dataFrame$absences, coercex, by = 35)
summary(dataFrame)#max 35
ggplot(dataFrame,aes(absences)) +geom_histogram(aes(position = 'fill'),color='black' ,bins=50 ,alpha=0.5) +theme_bw()

#absences affect on successs

convert<-function(x){ 
  if(x>11) return ('yes')
  return('no')}

success<-sapply(dataFrame$G3,convert)#create new colum
dataFrame$success<-success
dataFrame$success<-as.factor(dataFrame$success)
str(dataFrame)
ggplot(dataFrame,aes(absences)) + geom_histogram(aes(fill=success),color='black' ,bins=30 ,alpha=0.7) +theme_bw()

#failures affect on successs
ggplot(dataFrame,aes(failures)) + geom_histogram(aes(fill=success),color='black' ,bins=8 ,alpha=0.5) +theme_bw()



##Q3##Linear Regression


#corellation
dataFrame_num<-dataFrame# new df for corlleation
install.packages("corrplot")
require(corrplot)
install.packages("corrplot")
require(corrplot)
str(dataFrame_num)
dataFrame_num$school<-as.integer(dataFrame_num$school)
dataFrame_num$address<-as.integer(dataFrame_num$address)
dataFrame_num$sex<-as.integer(dataFrame_num$sex)
dataFrame_num$Mjob<-as.integer(dataFrame_num$Mjob)
dataFrame_num$Fjob<-as.integer(dataFrame_num$Fjob)
dataFrame_num$schoolsup<-as.integer(dataFrame_num$schoolsup)
dataFrame_num$nursery<-as.integer(dataFrame_num$nursery)
dataFrame_num$higher<-as.integer(dataFrame_num$higher)
dataFrame_num$success<-as.integer(dataFrame_num$success)
str(dataFrame_num)
mat<-cor(dataFrame_num)#store cor(df) in a matrix
corrplot(mat)#plot matrix



#1- split to training and testing 
install.packages('caTools')
library(caTools)

filter <- sample.split(dataFrame_num$studytime, SplitRatio = 0.7)

dataFrame_num.train <- subset(dataFrame_num, filter == TRUE)
dataFrame_num.test <- subset(dataFrame_num, filter == FALSE)

dim(dataFrame_num)
dim(dataFrame_num.train)
dim(dataFrame_num.test)


#2 linear regression model 
str(dataFrame_num)

model<-lm(G3 ~ .,dataFrame_num.train)

summary(model)# look the stars

predicted.train <- predict(model,dataFrame_num.train)
predicted.test <- predict(model,dataFrame_num.test)


MSE.train <- mean((dataFrame_num.train$G3- predicted.train)**2)
MSE.test <- mean((dataFrame_num.test$G3- predicted.test)**2)

RMSE.train <-MSE.train**0.5
RMSE.test <-MSE.test**0.5


#what is the mean final grade
m <- mean(dataFrame_num$G3) # 10.41

#error in percantage of the mean
per_error <-RMSE.test/m #40% prety large error 

#there is no overfitting (RMSE in test is similiar to train)


#3
predicted <- predicted.test>=11
predicted
actual<-dataFrame_num.test$G3>=11
cf<-table(predicted,actual)
cf

dim(dataFrame_num)
number.successes<- dim(dataFrame[dataFrame$success=='yes',])[1]
base_level <-number.successes/dim(dataFrame)[1]
base_level



##Q4##


percentile = dataFrame$G3  
quantile(percentile, c(.80)) # 14 is the minimum grade for exellemce

dataFrame$exellence<-dataFrame$G3>14# create new column
dataFrame$exellence<-as.factor(dataFrame$exellence)
summary(dataFrame$exellence)
str(dataFrame)


#divide into train and test set
library('caTools')

filter<-sample.split(dataFrame$age,SplitRatio = 0.7)

dataFrame.train<-subset(dataFrame,filter==T)
dataFrame.test<-subset(dataFrame,filter==F)

dim(dataFrame.test)
dim(dataFrame.train)



#2-Naive Bayes
install.packages('e1071')
library(e1071)
dim(dataFrame)


modelNB <- naiveBayes(exellence~.,dataFrame.train)
summary(modelNB)
prediction <- predict(modelNB,dataFrame.test, type = "raw")
predict <- prediction[,'TRUE']


actual <- dataFrame.test$exellence
predicted <- predict >0.5

cfNB <- table(predicted, actual)
cfNB

precision <-cfNB['TRUE','TRUE']/(cfNB['TRUE','TRUE'] + cfNB['TRUE','FALSE'] )
recall <- cfNB['TRUE','TRUE']/(cfNB['TRUE','TRUE'] + cfNB['FALSE','TRUE'] )
precision
recall

number_errorsNB<-(cfNB[1,2]+ cfNB[2,1])
number_errorsNB
total_errorsNB <-  (number_errorsNB)/dim(dataFrame.test)[1]
total_errorsNB


#3-Decision tree 

install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

#run the algorithm to produce a model
modelTR<-rpart(exellence~.-G3,dataFrame.train)
rpart.plot(model, box.palette="RdBu", shadow.col="gray", nn=TRUE)


#calculating the confusion matrix
predict.prob<-predict(modelTR,dataFrame.test)
predict.prob.yes <- predict.prob[,'TRUE']
predicted <- predict.prob.yes > 0.5
actual<-dataFrame.test$exellence



cfTR<- table(predicted,actual)
cfTR

#compute precision and recall 
precision <-cfTR['TRUE','TRUE']/(cfTR['TRUE','TRUE'] + cfTR['TRUE','FALSE'] )
recall <- cfTR['TRUE','TRUE']/(cfTR['TRUE','TRUE'] + cfTR['FALSE','TRUE'] )
precision
recall

precision
recall




#4- ROC curve 
install.packages('pROC')
library(pROC)

rocCurveTR <- roc( dataFrame.test$exellence, predict.prob.yes, direction = "<", levels = c("FALSE","TRUE"))
rocCurveNB <- roc( dataFrame.test$exellence, predict, direction = "<", levels = c("FALSE","TRUE"))


plot(rocCurveTR, col="red", main='ROC chart')
par(new=TRUE)
plot(rocCurveNB, col="blue", main='ROC chart')



#Calculate AUC
auc(rocCurveTR)
auc(rocCurveNB)











